**Open University TT284 Block 3 files**


*Demonstrations from tutorial on January 18th 2021*

---

## Upload CSV file into DB

1. import.upload.html.php is the HTML file that handles the UI
2. process.csv.ajax.php handles the writing to the DB
3. Needs a DB configured as per the db.inc.php file

This demo includes some helper functions that may be useful as well as showing how the PDO library works when working with databases. 


## Fetch

The /fetch folder has a number of files -


1. fetch.html is the container for the application
2. fetch.css handles the styling of the accordions (not discussed in the tutorial)
3. fetch.js handles the fetchAPI, pulling data from the remote 'server'
4. fetch.json is the data, structured with a key
6. I use the JSON formatter at https://jsonformatter.org to validate and prettify JSON.


copyright AW.Feb. 2021
