<?php
include_once 'includes/helpers.inc.php';
include_once 'includes/db.inc.php';
/* 
 * mySelection is a collection of potential column names.  We need to check each one to see if it is a valid column name from the table
 * we are planning to INSERT INTO or UPDATE
*/


$myData = json_decode($_POST['csvdata']);
echo $_POST['csvdata'];

$mySelection = (isset($_POST['csv']) ? $_POST['csv'] : '');
debug($mySelection, 0);
$validColumns = getColumns($pdo, 'tma02_demo');
debug($validColumns, 0);


function bindFields($fields, $validColumns){
    end($fields); $lastField = key($fields);
    $bindString = ' ';
    foreach($fields as $field => $data){ 
        if (in_array($field, $validColumns)) {
            $bindString .= $field . ' = :' . $field; 
            $bindString .= ($field === $lastField ? ' ' : ',');
        }
    }
    
    return rtrim($bindString,',');
}

function bindValues(&$s, $fields, $validColumns){
    end($fields); $lastField = key($fields);    
    foreach($fields as $field => $data){ 
        if (in_array($field, $validColumns)) {
            $term = $data;
            $s->bindValue(":$field", $term); 
            
        }
    }    
}

if ($mySelection <> '') {
    echo '<ul>';
    foreach ($myData as $myCourse) {
        echo '<li class="text-success">Course - ';        
        //$update = 0;
        // Create an array of terms to be inserted/updated
        $doPathway = 0;
        
        foreach ($mySelection as $column) {            
            debug($myCourse->$column, 0);

            if ($column == 'code') {                             
                debug('Checking ' . $myCourse->$column, 0);
                try {
                    $sql = "SELECT ID FROM tma02_demo WHERE code = :code";
                    $s = $pdo->prepare($sql);
                    $s->bindValue(':code', $myCourse->$column);
                    $s->execute();
                    $ID = $s->fetchColumn();
                    if ($s->rowCount() > 0) {
                        $update = 1;
                    } else {
                        $update = 0;
                    }
                } catch (Exception $ex) {
                    $error = $ex->getMessage();
                    include 'error.html.php';
                }
            }
        }
        
        // Either INSERT or UPDATE depending on whether the code is set (this must be unique)
        if ($update == 1) {
            $sql = "UPDATE tma02_demo SET " . bindFields($myCourseImportTerms, $validColumns) . ' WHERE ID = :ID';        
            $res = $pdo->prepare($sql); 
            $res->bindValue(':ID', $ID);
            bindValues($res,$myCourseImportTerms, $validColumns);
                   
            $res->execute();                        
            echo (' :Updated with ID = ' . $ID . '. ');            
        } else {            
            $sql = "INSERT INTO tma02_demo SET " . bindFields($myCourseImportTerms, $validColumns);        
            $res = $pdo->prepare($sql); 
            bindValues($res, $myCourseImportTerms, $validColumns);
            $res->execute();
            $ID = $pdo->lastInsertId();
            
            echo (' :Added with ID = ' . $ID . '. ');
        }    
    }
    echo '</ul>';
}