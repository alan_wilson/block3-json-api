<!DOCTYPE html>
<!--
Prototype for importing a CSV file, selecting some header columns and importing data into table
-->
<html lang="en">
    <head>
        <title>Import a CSV File</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/tutorial2.css">
        <script type="text/javascript" src="js/jquery.csv.min.js"></script>
        <script type="text/javascript">
            
        // Bit of jQuery to do the hard work    
        $( function() {

            // Get the csvFileUpload element and attach an event listener on change
            document.getElementById('csvFileUpload').addEventListener('change',upload,false);
            
            function browserSupportUpload() {
                var compatible = false;
                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    compatible = true
                }
                return compatible
            }           
            
            function upload(e) {
                console.log('Upload event called');  // Called when the button is clicked.
                if (!browserSupportUpload) {
                    alert('Upgrade your browser');
                } else {
                    //console.log('OK');
                    var data = null;
                    var file = e.target.files[0];
                    var reader = new FileReader();
                    reader.readAsText(file);
                    reader.onload = function (event) {
                        // Get the data using the jQuery csv library
                        var csvData = event.target.result;
                        rawdata = $.csv.toArrays(csvData);
                        data = $.csv.toObjects(csvData); // Create an Object from the CSV data

                        // If we got some data
                        if (data && data.length > 0) {

                            var fileInfo = '';  // Create a variable to display some HTML
                            fileInfo += '<span class="text-primary"> - File Type: ' + (file.type || 'n/a')  + '<br></span>';
                            fileInfo += '<span class="text-primary"> - File Size: ' + file.size + ' bytes'  + '<br></span>';
                            fileInfo += '<span class="text-primary"> - Last Modified: ' + (file.lastModified ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br></span>';
                            
                            column = rawdata[0];
                            // Assume first row is header field
                            var databaseColumns = new Object;  // Create a new object for the columns
                            var csvcolumnhtml = '';
                            
                            for (counter=0;counter<rawdata[0].length;counter++) {
                                item = column[counter];                                
                                if ((item.length > 0) && (!databaseColumns[item])) {
                                    databaseColumns[item] = 'Y';
                                    csvcolumnhtml += '<div><input type="checkbox" name="csv[]" value="' + item + '">' + item + '</div>';                                
                                }
                            }
                            
                            csvbuttonhtml = '<div class="col-md-1"><button class="btn btn-success btn-xs" id="checkall" value="checkall" name="submit" type="submit">Select All</button></div>';
                            csvbuttonhtml += '<div class="col-md-1 col-md-offset-2"><button class="btn btn-danger btn-xs" id="clearall" value="clearall" name="submit" type="submit">Select None</button></div>';
                            
                            //console.log(databaseColumns);
                            jsonData = JSON.stringify(data); // Turn our Object into an array for passing to backend script via AJAX.
                            $('#file_details').html(fileInfo);
                            $("#process").attr('disabled',false); 
                            $('#csvcolumns').html(csvcolumnhtml);
                            $('#csvbuttons').html(csvbuttonhtml);
                                                       
                        } else {
                            console.log('No data found.');
                        }
                        
                    };
                }                
            }
            

            // Use jQuery to process the click of the submit button that lives in the container
            $('.container').on("click", 'button[type="submit"]',function(e) {
                e.preventDefault();  // Prevent the form being submitted.
                //console.log ('Button clicked ' + this.id); 
                

                // Take action depending on the id of the button
                if (this.id === 'process') {
                    var csv = $(":checkbox:checked").map(function(){ 
                        return $(this).val();
                    }).get();

                    return $.ajax({
                        type: "POST",
                        url: "process.csv.ajax.php",
                        data: {                        
                            'csvdata': jsonData,
                            'csv': csv
                        },
                        success: function(dataresult) {                   
                             $("#calc_result").html(dataresult);
                         }
                    });                
                }
                
                // Clear the checkboxes etc.
                if (this.id === 'clear') {                    
                    var form = document.getElementById('uploadform');
                    form.reset();
                    $("#process").attr('disabled',true);
                    $('#csvcolumns').html('');
                    $('#file_details').html(''); 
                    $('#calc_result').html(''); 
                }
                
                if (this.id === "clearall") {
                    var autoenrol = $('#autoenrol').prop('checked');
                    var studentItems = $(":checkbox").map(function(){ 
                        $(this).prop('checked',false);
                    });
                    $('#autoenrol').prop('checked',autoenrol); // Exclude this one from the global update
                               
                }

                // Select all the CSV check boxes
                if (this.id === "checkall") {  
                    var autoenrol = $('#autoenrol').prop('checked');
                    //console.log(autoenrol);
                    var studentItems = $(":checkbox").map(function(){ 
                        //$(this).removeAttr('checked');
                        $(this).prop('checked',true);                        
                    });                
                    $('#autoenrol').prop('checked',autoenrol); // Exclude this one from the global update
                }
                
            });
              
            
        });

        </script>                    
        
    </head>
    <body>
        <div class="container">
       
        <form class="form" id="uploadform">
        <div id="importCSV" class="fileUpload">
            <fieldset>
                <legend>
                    Upload CSV file:
                </legend>
                <input type="file" name="File Upload" id="csvFileUpload" accept=".csv"/>
            </fieldset>
        </div>        
            
        <div id="file_details" class="bg-success"></div>        
        
            <fieldset>
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <div class="checkbox" id="csvcolumns"></div>                    
                    </div>                    
                </div>
                <div class="row">                    
                    <span id="csvbuttons" class="pull-left"></span>                    
                </div>
            </fieldset>
            <hr>

            <button class="btn btn-primary" type="submit" name="process" id="process" disabled>Process</button>
            <button class="btn btn-primary" type="submit" name="clear" id="clear">Clear</button>
            Auto-enrol&nbsp;<input type="checkbox" name="enrol" value="Enrol" id="autoenrol">        
 
    <div id="calc_result"></div>    
    </form>
    </div>
    </body>
</html>
