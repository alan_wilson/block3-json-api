<?php

function html($text)
{
    if (! is_array($text)) {
        return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
    }
      
}

function htmlout($text)
{
  echo html($text);
}

function markdown2html($text)
{
  $text = html($text);

  // strong emphasis
  $text = preg_replace('/__(.+?)__/s', '<strong>$1</strong>', $text);
  $text = preg_replace('/\*\*(.+?)\*\*/s', '<strong>$1</strong>', $text);

  // emphasis
  $text = preg_replace('/_([^_]+)_/', '<em>$1</em>', $text);
  $text = preg_replace('/\*([^\*]+)\*/', '<em>$1</em>', $text);

  // Convert Windows (\r\n) to Unix (\n)
  $text = str_replace("\r\n", "\n", $text);
  // Convert Macintosh (\r) to Unix (\n)
  $text = str_replace("\r", "\n", $text);

  // Paragraphs
  $text = '<p>' . str_replace("\n\n", '</p><p>', $text) . '</p>';
  // Line breaks
  $text = str_replace("\n", '<br>', $text);

  // [linked text](link URL)
  $text = preg_replace(
      '/\[([^\]]+)]\(([-a-z0-9._~:\/?#@!$&\'()*+,;=%]+)\)/i',
      '<a href="$2">$1</a>', $text);

  return $text;
}

function markdownout($text)
{
  echo markdown2html($text);
}


function fix_string($string) {
    if (get_magic_quotes_gpc()) $string = stripslashes($string);
    return htmlentities ($string);
}


function createTable($name, $query) {
    global $pdo;
    try {
        $sql = "CREATE TABLE IF NOT EXISTS $name ($query)";
        $c_table = $pdo->prepare($sql);
        $c_table->execute();
        return 0;
    } catch (PDOException $e) {  
        echo "Error creating table '$name'." . $e->getMessage(). "<br>";
        return 1;
    }
}

function describeTable($table) {
    global $pdo;
    try {
         $sql = "DESCRIBE $table";
         $d_table = $pdo->prepare($sql);
         $d_table->execute();
    } catch (PDOException $e) {
        echo "Error describing table";
    }
   
    $result = $d_table->fetchAll(PDO::FETCH_ASSOC);
       
    $describeString = '<div class="alert alert-success alert-dismissible" role="alert">';
    $describeString .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    $describeString .= '<span class="lead text-info">Schema: <br></span><div class="row"><div class="col-md-6">Field</div><div class="col-md-6">Type</div></div>';    
    
    foreach ($result as $row)
    {
        $describeString .= '<div class="row">';
        $describeString .= '<div class="col-md-6">' . stripslashes($row['Field']) . '</div>';
        $describeString .= '<div class="col-md-6">' . stripslashes($row['Type']) . '</div>';
        $describeString .= '</div>';
    }
    
    $describeString .= '</div>';
    return $describeString;
}

function debug($string, $flag)  {
    /* Prints out a string, either var_dump or print_r
     * 0 or 1 for the flag
     */

    echo '<pre>';
    if ( $flag === 0) {
        print_r ($string);
    } else {
        var_dump ($string);
    }
    echo '</pre>';
}

function getColumns($pdo, $table)
{
    // Generic function to get a list of columns and return them as a collection (array)
    //global $pdo;
    $these_columns = [];
    try {
        $sql = 'SHOW COLUMNS FROM ' . $table;
        $s = $pdo->prepare($sql);
        $s->execute();
        $res = $s->fetchAll(PDO::FETCH_ASSOC);
        foreach ($res as $column) {            
            $these_columns[] = $column['Field'];            
        }
        return $these_columns;
    } catch (Exception $ex) {
        $error = "Error extracting column information.";
        include 'error.html.php';
        return false;
    }
}