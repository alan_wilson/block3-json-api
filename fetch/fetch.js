/* 
 * JavaScript to drive a simple accordion using fetch API
 * Grabs the same data two different ways for demo.
 */

var message = document.querySelector("#message");

// First we use this fetch 
fetch('http://localhost/tutorial2/fetch/fetch.json') 
    .then(function (response) {

        console.table(response);  // Some debug
        
        if (response.ok) {
            
            response.json().then(function(payload) {

                var myData = Object.values(payload.concerts);

                // console.log(myData);
                
                for (i=0;i<myData.length;i++) {
                    
                    console.log('Calculating dates');
                    
                    var myDate = myData[i]["time"];                    
                    var d = new Date(myDate*1000);
                    console.log(d.toDateString());      // Log the proper date for checking            
                    
                    // Update the page                  
                    message.insertAdjacentHTML ('beforeEnd','<h3 class="accordion">' + myData[i]["title"]) + '</h3>';
                    message.insertAdjacentHTML ('beforeEnd','<div class="content"><p>' +  d.toDateString()) + '</p></div>';
                            
                }
                
              });
           
                      
        } else {
            
            console.warn("Response was not OK:", response.status);
    
        }
        
    })



function checkResponse(response) {   
    // Check to see if the correct code is returned by the fetch
    if (!response.ok) {
      
        throw Error(response.statusText);
    
    }
  
    return response;  
}


function showText(responseAsText) {

    console.log('showText called');
    console.log(responseAsText.concerts); //  Log the entities we received

    var message = document.getElementById('message')

    for (var key in responseAsText.concerts) {

        message.insertAdjacentHTML ('beforeEnd','<h3 class="accordion">' + responseAsText.concerts[key].title) + '</h3>';
        message.insertAdjacentHTML ('beforeEnd','<div class="content"><p>' + responseAsText.concerts[key].time) + '</p></div>';

    }

    workAccordions();  // Function to generate the accordions
}


function workAccordions() {
    
    var accordion = document.getElementsByClassName('accordion');
    console.log('Working the accordion - ' + accordion.length + ' items');

    /* 
     * Algorithm is 
     * Loop round the class elements (3 in the demo material)
     *   if element clicked
     *      toggle active class
     *      get the next sibling (the section with the text in it)
     *      if it's expanded
     *          close it
     *      else
     *          open it
     *  end loop   
    */
    for (var index=0;index<accordion.length;index++) {

        accordion[index].onclick = function() {

            this.classList.toggle("active");  // Toggle this accordion class to/from active

            var section = this.nextElementSibling;

            if (section.style.maxHeight) {

                section.style.maxHeight = null; // Collapse it by removing the maxHeight style altogether, setting to 0px doesn't work !
                
            } else {

                section.style.maxHeight = section.scrollHeight + 'px'; // Set it to the height of the section as per the text it contains.

            }

        }

    }
    
}


// Second approach is checking the response and chaining, calling the showText() function
fetch('http://localhost/tutorial2/fetch/fetch.json') 
    .then(checkResponse)
    .then(res => res.json())
    .then(json => showText(json));